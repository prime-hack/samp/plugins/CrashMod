#include <windows.h>

void nop( size_t address, size_t len ) {
	DWORD oldVP;

	VirtualProtect( (void *)address, len, PAGE_EXECUTE_READWRITE, &oldVP );
	memset( (void *)address, 0x90, len );
	VirtualProtect( (void *)address, len, oldVP, &oldVP );
}

BOOL APIENTRY DllMain( HMODULE, DWORD, LPVOID ) {
	auto samp_dll = (DWORD)GetModuleHandleA( "samp" );
	auto isR1	  = *(unsigned char *)( samp_dll + 0x129 ) == 0xF4;

	nop( samp_dll + ( isR1 ? 0x5CC65 : 0x60005 ), 2 );

	return FALSE;
}
